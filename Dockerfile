FROM ubuntu
MAINTAINER Kit Barnes <kit@ninjalith.com>
EXPOSE 5000

RUN DEBIAN_FRONTEND=noninteractive apt-get -y update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y python-pip python-dev

COPY sup.py /opt/sup/sup.py
COPY requirements.txt /opt/sup/requirements.txt

RUN pip install -r /opt/sup/requirements.txt

ENTRYPOINT ["/opt/sup/sup.py"]
CMD []
